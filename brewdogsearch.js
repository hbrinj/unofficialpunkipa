const https = require('https');

exports.handler = function (event, context) {
    try{
        console.log("event.session.application.applicationID=" + event.session.application.applicationId);
    
        if(event.session.new) {
            onSessionStarted({requestId: event.request.requestId}, event.session);
        }
        
        switch(event.request.type) {
            case "LaunchRequest":
                onLaunch(
                    event.request, 
                    event.session,
                    (sessionAttr, speechletResponse) => {
                        context.succeed(buildResponse(sessionAttr, speechletResponse));
                    }
                );
                break;
            case "IntentRequest":
                onIntent(
                    event.request, 
                    event.session,
                    (sessionAttr, speechletResponse) => {
                        context.succeed(buildResponse(sessionAttr, speechletResponse));
                    }
                );
                break;
            case "SessionEndedRequest":
                onSessionEnded(event.request, event.session);
                context.succeed();
                break;
        }
    } catch (e) {
        context.fail("Exception: "+e);
    }
};

function onSessionStarted(sessionStartedRequest, session) {
    console.log("onSessionStarted requestId="+sessionStartedRequest.requestId+", sessionId="+session.sessionId);
}

function onLaunch(launchRequest, session, callback) {
    console.log("onLaunch requestId="+launchRequest.requestId+", sessionId="+session.sessionId);
    
    var cardTitle = "BrewdogSearch",
        speechOutput = "Hello, you can search for brewdog beers and their food pairings or say exit to leave";
    
    callback(session.attributes, buildSpeechletResponse(cardTitle, speechOutput, "", false));
}

function onIntent(intentRequest, session, callback) {
    console.log("onIntent requestId="+intentRequest.requestId+", sessionId="+session.sessionId);

    var intent = intentRequest.intent;

    switch(intentRequest.intent.name) {
        case 'BrewdogSearch':
            handleBeerSearch(intent, session, callback);
            break;
        case 'AMAZON.HelpIntent':
            callback(session.attributes,
                buildSpeechletResponseWithoutCard(
                    "To use brewdog search try, give me a random beer. To find a beer to go with your meal try, what beer goes well with spaghetti. To find food that goes with your beer try, what food goes well with dead pony club. To search for beers based on their alcohol content try, search for beers with an alcohol content less than 6. What would you like to do?",
                    "", 
                    "false")
                );
            break;
        case 'AMAZON.CancelIntent': 
            callback(session.attributes,buildSpeechletResponseWithoutCard("Canceling...","", "true"));
            break;
        case 'AMAZON.StopIntent': 
            callback(session.attributes,buildSpeechletResponseWithoutCard("Stopping...","", "true"));
            break;
        default:
            throw "invalid intent";
    }
}

function onSessionEnded(sessionEndedRequest, session) {
    console.log("onSessionEnded requestId="+sessionEndedRequest.requestId+", sessionId="+session.sessionId);
}

function handleBeerSearch(intent, session, callback) {
    console.log("handleProjectSearch sessionId="+session.sessionId);
    var response_func = null,
        limit = 20;
    var config = {
        host: 'api.punkapi.com',
        headers: {
            'User-Agent':"UnofficialBrewdogSearch",
            'Content-Type':"application/json"
        }
    };

    if( intent.slots.ABVMore.hasOwnProperty("value") ) {
         config.path = "/v2/beers?abv_gt="+intent.slots.ABVMore.value;
         response_func = buildBeerText;
    } else if ( intent.slots.ABVLess.hasOwnProperty("value") ) {
         config.path = "/v2/beers?abv_lt="+intent.slots.ABVLess.value;
         response_func = buildBeerText;
    } else if( intent.slots.ComplBeername.hasOwnProperty("value")) {
        config.path = "/v2/beers?beer_name="+encodeURIComponent(intent.slots.ComplBeername.value);
        response_func = buildComplFoodText;
    } else if( intent.slots.ComplFoodname.hasOwnProperty("value")) {
        config.path = "/v2/beers?food="+encodeURIComponent(intent.slots.ComplFoodname.value);
        response_func = buildComplBeerText;
    } else if( intent.slots.Beername.hasOwnProperty("value") ) {
        config.path = "/v2/beers?beer_name="+encodeURIComponent(intent.slots.Beername.value);
        response_func = buildBeerInfoText;
    } else {
        config.path = "/v2/beers/random";
        response_func = buildRandomBeerText;
    }

    if( intent.slots.Limit.hasOwnProperty("value") ) {
        limit = intent.slots.Limit.value;
    }
    send_request(config, callback, session, response_func, limit);
    return;
}

//-----------------------Helpers----------------------

function send_request(config, callback, session, response_func, limit) {
    console.log(config.path);
    
    https.get(config, (res) => {
        res.setEncoding('binary');
        const stat = res.statusCode;
        const contentType = res.headers['content-type'];
        
        let error;
        let raw = '';

        if(stat !== 200) {
            error = new Error('Request Failed. ' + `StatusCode: ${stat}`);
        } else if (!/^application\/json/.test(contentType)) {
            error = new Error('Invalid content-type \n' + `Expected application/json but recieved ${contentType}`);
        }
        if(error) {
            console.error(error.message);
            res.resume();
            callback(session.attributes,
                buildSpeechletResponseWithoutCard("Sorry, It looks like the search failed, please try again","", "true")
            );
            return;
        }

        res.on('data', (chunk) => {raw += chunk;});
        res.on('end', () => {
            try{
                const parsedData = JSON.parse(raw);
                callback(session.attributes, 
                    buildSpeechletResponseWithoutCard(
                        response_func(limit,parsedData),
                        "", "true"
                    )
                );
            } catch (e) {
                console.error(e.message);
            }
        });
    });
}

function buildBeerText(limit, response) {
    if( response.length === 0 ) {
        return "No beers were found";
    }

    if(response.length < limit) {
        limit = response.length;
    }

    var text = "Here are the beers I found. ";

    for(i = 0; i < limit; i++) {
        text += response[i].name;
        if(i < limit-1) {
            text += ", ";
        }
    }
    return text;
}

function buildRandomBeerText(limit, response) {
    return "The beer I found was: " + response[0].name + ", the " + response[0].tagline;
}

function buildBeerInfoText(Limit, response) {
    if(response.length === 0) {
        return "A beer by that name was not found";
    }
    var text = "" + response[0].name + ", " + response[0].description;
    return response[0].name + ", " + response[0].description;
}

function buildComplFoodText(limit, response) {
    if( response.length === 0 ) {
        return "No foods were found";
    }

    var count = 0;
    for(var i = 0; i < response.length; i++) {
        count += response[i].food_pairing.length;
    }
    if(count < limit) {
        limit = count;
    }

    var text = "Here are the complementing foods I found. ";
    var respcount = response.length;
    var j = 0;
    while(j < respcount && limit > 0)
    {
        var i = 0;
        console.log(response[j].food_pairing.length);
        console.log("---------------Limit is:" + limit);
        while(i < (response[j].food_pairing.length) && limit > 0) {
            text += response[j].food_pairing[i];
            console.log(text);
            limit--;
            if(limit > 0) {
                text += ",";
            }
            console.log(i);
            i++;
        }
        j++;
    }
    return text;
}

function buildComplBeerText(limit, response) {
    if( response.length === 0 ) {
        return "No beers were found";
    }

    if(response.length < limit) {
        limit = response.length;
    }

    var text = "Here are the complementing beers I found. ";

    for(i = 0; i < limit; i++) {
        text += response[i].name;
        if(i < limit-1) {
            text += ", ";
        }
    }
    return text;
}
function buildSpeechletResponse(title, output, repromptText, shouldEndSession) {
    return {
        outputSpeech: {
            type: "PlainText",
            text: output
        },
        card: {
            type: "Simple",
            title: title,
            content: output
        },
        reprompt: {
            outputSpeech: {
                type: "PlainText",
                text: repromptText
            }
        },
        shouldEndSession: shouldEndSession
    };
}


function buildSpeechletResponseWithoutCard(output, repromptText, shouldEndSession) {
    return {
        outputSpeech: {
            type: "PlainText",
            text: output
        },
        reprompt: {
            outputSpeech: {
                type: "PlainText",
                text: repromptText
            }
        },
        shouldEndSession: shouldEndSession
    };
}

function buildResponse(sessionAttr, speechletResponse) {
    return {
        version: "1.0",
        sessionAttributes: sessionAttr,
        response: speechletResponse
    }
}
function containsNonLatinCodepoints(s) {
        return /[^\x00-\x7F]/.test(s);
}
